/*!
 *  JSON2Plain Converter.
 *
 *  @author Jarrad Seers <jarrad@jarradseers.com>
 *  @created Fri 21 Dec 2012 01:25:58 NZDT
 */

// This tool is taken from https://github.com/jarradseers/json2plain repository. It is licensed under MIT license by the creator of this tool.
// I modified it slightly to match our use case in this addon.
// Unminifed version below :

(function() {
  var JSON2Plain = function(json, options) {
    var options = options || {};

    if (typeof json !== "string") {
      json = JSON.stringify(json);
    }

    json = JSON.parse(json);

    this.depth = options.depth || 1;
    this.newline = options.newline || "\n";
    this.indent = options.indent || "  ";
    this.separator = options.separator || ": ";
    //this.list       = options.list      || '- ';
    this.list = "numbered";

    this.output = options.prefix || "\n";

    this.formatKey = options.formatKey || this.ucFirst;
    //this.formatVal  = options.formatVal || this.ucFirst;

    this.__process(this.depth, json);
    this.output += options.suffix || "\n";

    return this;
  };

  /**
   *  Format indentation based on depth.
   *
   *  @param depth {Number} - indentation depth.
   *  @returns JSON2Plain {Object} - this instance.
   */

  JSON2Plain.prototype.__format = function(depth) {
    for (var d = 0; d < depth; d++) {
      this.output += this.indent;
    }

    return this;
  };

  /**
   *  Uppercase first method.
   *
   *  @param string {String} - string to uppercase first char.
   *  @returns {String} - uppercased first character.
   */

  JSON2Plain.prototype.ucFirst = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  /**
   *  Recursive processing.
   *
   *  @param depth {Number} - indentation depth.
   *  @param json {Object} - valid JSON object.
   *  @returns JSON2Plain {Object} - this instance.
   */

  JSON2Plain.prototype.__process = function(depth, json) {
    var isArray = Array.isArray(json);
    var count = 0;

    for (var key in json) {
      if (count !== 0 || depth !== this.depth) {
        this.output += this.newline;
      }

      this.__format(depth);
      if (this.list.toLowerCase() !== "numbered" && !isNaN(key)) {
        this.output += this.list;
      } else if (!isArray) {
        this.output += this.formatKey(key.toString()) + this.separator;
      } else if (isArray) {
        this.output +=
          this.formatKey((Number(key) + 1).toString()) + this.separator;
      }

      if (typeof json[key] === "object") {
        this.__process(depth + 1, json[key]);
      } else {
        this.output += json[key].toString();
      }

      count++;
    }

    return this;
  };

  /**
   *  Expose the JSON2Plain.
   *
   *  @param depth {Number} - indentation depth.
   *  @param json {Object} - valid JSON object.
   *  @returns output {String} - Plain text.
   */

  window.JSON2Plain = function(json, options) {
    return new JSON2Plain(json, options).output;
  };
})();
