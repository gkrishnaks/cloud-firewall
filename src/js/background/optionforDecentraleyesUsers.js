/*******************************************************************************

    
    Cloud Firewall - a browser extension/addon that allows users to block connections 
    to sites, pages and web resources (images, videos, etc) hosted in major cloud services
    if the user wishes to do so. 
    Copyright (C) 2019 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/cloud-firewall
*/

// This is a feature request from a user who also has the fantastic Decentraleyes addon
// Provide a feature option for Decentraleyes users so that Decentraleyes provide some
// ..CDN resources from its local instead of making network call i.e Decentraleyes acts as offline CDN
// https://decentraleyes.org/test/
// To test :
// https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js

function isSupportedByDecentraleyes(hostname) {
  // Thought of reusing the Decentraleyes logic here for supported versions of libraries.
  // But Decentraleyes already has "Block CDN which are NOT supported by Decentraleyes"
  // An advanced user using Decentraleyes can enable that option in Decentraleyes if she wishes to avoid passthrough.
  // I am just going to search for hostnames and return, and let Decentraleyes addon take care further.

  let decentralEyesSupportedHosts = [
    "ajax.googleapis.com",
    "ajax.aspnetcdn.com",
    "ajax.microsoft.com",
    "cdnjs.cloudflare.com",
    "code.jquery.com",
    "cdn.jsdelivr.net",
    "yandex.st",
    "yastatic.net",
    "apps.bdimg.com",
    "libs.baidu.com",
    "lib.sinaapp.com",
    "upcdn.b0.upaiyun.com",
    "cdn.bootcss.com",
    "sdn.geekzu.org",
    "ajax.proxy.ustclug.org"
  ];

  if (decentralEyesSupportedHosts.includes(hostname)) {
    return true;
  }

  return false;
}
