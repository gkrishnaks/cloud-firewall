/*******************************************************************************

    
    Cloud Firewall - a browser extension/addon that allows users to block connections 
    to sites, pages and web resources (images, videos, etc) hosted in major cloud services
    if the user wishes to do so. 
    Copyright (C) 2019 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/cloud-firewall
*/

//This file is for future release of addon if it's feasible to implement AES256 encryption of cache

async function encryptCache(password) {
  var rounds = 5000;
  /* if (counter < 5000) {
    rounds = 5000;
  } else {
    rounds = counter;
  }*/

  //future version will get rounds from user.

  var iv = forge.random.getBytesSync(12);

  var salt = forge.random.getBytesSync(128);
  var encoder = new TextEncoder();

  var key = forge.pkcs5.pbkdf2(password, salt, rounds, 32, "sha512");
  var ivHex = forge.util.bytesToHex(iv);
  var saltHex = forge.util.bytesToHex(salt);
  await browser.storage.local.set({ iv: ivHex });
  await browser.storage.local.set({ salt: saltHex });

  var encoder = new TextEncoder();
  // encrypt some bytes using GCM mode

  var cipher = forge.cipher.createCipher("AES-GCM", key);
  cipher.start({
    iv: iv, // should be a 12-byte binary-encoded string or byte buffer
    //additionalData: 'binary-encoded string', // optional
    tagLength: 128 // optional, defaults to 128 bits
  });
  // cipher.update(forge.util.createBuffer(encoder.encode(JSON.stringify(cache))));
  cipher.update(forge.util.createBuffer(encoder.encode("test string here")));

  cipher.finish();
  var encrypted = cipher.output;
  var tag = cipher.mode.tag;
  await browser.storage.local.set({ tag: tag });
  // outputs encrypted hex
  //console.log(encrypted.toHex().length);
  var bytes = encrypted.getBytes();
  var b64 = forge.util.encode64(bytes);
  console.log(b64.length / 1000 + " kb in base64 of cipher blob");
  var compressed = LZString.compressToUTF16(b64);
  console.log(compressed.length / 1000 + " kb in base64 of compressed");
  await browser.storage.local.set({ cache: compressed });
  decryptCache("paswrod");
}

async function decryptCache(password) {
  console.log("in decryptcache");
  var saltHex = await browser.storage.local.get("salt");
  var ivhex = await browser.storage.local.get("iv");
  var tag = await browser.storage.local.get("tag");
  // decrypt some bytes using GCM mode
  var keyfromHex = forge.pkcs5.pbkdf2(
    password,
    forge.util.hexToBytes(saltHex),
    5000,
    32,
    "sha512"
  );

  var decipher = forge.cipher.createDecipher("AES-GCM", keyfromHex);
  decipher.start({
    iv: forge.util.hexToBytes(ivhex),
    tagLength: 128, // optional, defaults to 128 bits
    tag: tag // authentication tag from encryption
  });
  decipher.update(
    forge.util.createBuffer(
      forge.util.decode64(LZString.decompressFromUTF16(compressed))
    )
  );
  var pass = decipher.finish();

  // pass is false if there was a failure (eg: authentication tag didn't match)
  var decoder = new TextDecoder();
  if (pass) {
    // outputs decrypted hex
    var result = {};

    result.data = JSON.parse(decipher.output);

    /* console.log(JSON.parse(decipher.output.bytes()).length);
  console.log(typeof decipher.output.bytes());
  console.log(decipher.output.bytes())*/
    //https://web.archive.org/web/20180704233908/http://joelinoff.com/blog/?p=1614
    // https://github.com/digitalbazaar/forge/issues/128
    //decipher.output.getBytes()
    console.log(result);
    return result;
  } else {
    console.log("decrypt failed");
  }
}

function encrypttest(counter, data) {
  console.log(JSON.stringify(data).length / 1000 + " kb initial pre-encrypt");
  var rounds = 5000;
  if (counter < 5000) {
    rounds = 5000;
  } else {
    rounds = counter;
  }

  var iv = forge.random.getBytesSync(12);

  var salt = forge.random.getBytesSync(128);
  var encoder = new TextEncoder();

  var key = forge.pkcs5.pbkdf2("SomePasswordHere", salt, rounds, 32, "sha512");
  var ivHex = forge.util.bytesToHex(iv);
  var saltHex = forge.util.bytesToHex(salt);
  var encoder = new TextEncoder();
  // encrypt some bytes using GCM mode

  var cipher = forge.cipher.createCipher("AES-GCM", key);
  cipher.start({
    iv: iv, // should be a 12-byte binary-encoded string or byte buffer
    //additionalData: 'binary-encoded string', // optional
    tagLength: 128 // optional, defaults to 128 bits
  });
  cipher.update(forge.util.createBuffer(encoder.encode(JSON.stringify(data))));
  cipher.finish();
  var encrypted = cipher.output;
  var tag = cipher.mode.tag;

  // outputs encrypted hex
  //console.log(encrypted.toHex().length);
  var bytes = encrypted.getBytes();
  var b64 = forge.util.encode64(bytes);
  console.log(b64.length / 1000 + " kb in base64 of cipher blob");
  var compressed = LZString.compressToUTF16(b64);
  console.log(compressed.length / 1000 + " kb in base64 of compressed");

  var keyfromHex = forge.pkcs5.pbkdf2(
    "SomePasswordHere",
    forge.util.hexToBytes(saltHex),
    rounds,
    32,
    "sha512"
  );

  //console.log(forge.util.encode64(encrypted))

  // decrypt some bytes using GCM mode
  var decipher = forge.cipher.createDecipher("AES-GCM", keyfromHex);
  decipher.start({
    iv: iv,
    tagLength: 128, // optional, defaults to 128 bits
    tag: tag // authentication tag from encryption
  });
  decipher.update(
    forge.util.createBuffer(
      forge.util.decode64(LZString.decompressFromUTF16(compressed))
    )
  );
  var pass = decipher.finish();

  /* if sha1 :
L9ThxnotKPzthJ7hu3bnORuT6xI= encryption3.js:54:1
2fd4e1c67a2d28fced849ee1bb76e7391b93eb12 

if sha256 
16j7swfXgJRpypq8sAguT41WUeRtPNt2LQLQvzfJ5ZI= encryption3.js:54:1
d7a8fbb307d7809469ca9abcb0082e4f8d5651e46d3cdb762d02d0bf37c9e592

*/

  // pass is false if there was a failure (eg: authentication tag didn't match)
  var decoder = new TextDecoder();
  if (pass) {
    // outputs decrypted hex
    var result = {};

    result.data = JSON.parse(decipher.output);

    /* console.log(JSON.parse(decipher.output.bytes()).length);
  console.log(typeof decipher.output.bytes());
  console.log(decipher.output.bytes())*/
    //https://web.archive.org/web/20180704233908/http://joelinoff.com/blog/?p=1614
    // https://github.com/digitalbazaar/forge/issues/128
    //decipher.output.getBytes()
    console.log(result);
    return result;
  }
}

/* 15.366 kb initial pre-encrypt encryption3.js:2:1
20.488 kb in base64 of cipher blob encryption3.js:33:1
9.641 kb in base64 of compressed encryption3.js:35:1
163  */
