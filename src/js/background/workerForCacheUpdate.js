/*******************************************************************************

    
    Cloud Firewall - a browser extension/addon that allows users to block connections 
    to sites, pages and web resources (images, videos, etc) hosted in major cloud services
    if the user wishes to do so. 
    Copyright (C) 2019 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/cloud-firewall
*/
importScripts("../../libraries/ipaddr.min.js");
importScripts("../../libraries/ip-range-check.js");
importScripts("../data/CIDRblocks.js");

var CacheUpdateHelper = {};

// This worker is needed for this reason - we only search within the IP CIDR blocks of enabled blockers.
// Example: So user can have Facebook disabled and browse facebook -
// during that time, facebook hostnames will be stored in cache into "notInList" array.
// We have this worker for updating the cache in non-blocking way for moving the
// facebook specific sha256 hashes into facebook array in cache from "notInList"array, in this example.

CacheUpdateHelper.handleCacheToggle = (cache, settingsToggles, tempIpArray) => {
  var done = false;
  /* console.log(cache.facebook.hosts);
  console.log(cache.notInList.hosts);
  console.log(tempIpArray);*/
  settingsToggles.apple = !settingsToggles.apple;
  settingsToggles.facebook = !settingsToggles.facebook;
  settingsToggles.amazon = !settingsToggles.amazon;
  settingsToggles.microsoft = !settingsToggles.microsoft;
  settingsToggles.google = !settingsToggles.google;
  settingsToggles.cloudflare = !settingsToggles.cloudflare;

  for (let company in settingsToggles) {
    if (settingsToggles[company]) {
      for (let el of tempIpArray) {
        var result = CacheUpdateHelper.checkIPisinRange(el.ip, company);
        if (result.whichCompany !== "notInList") {
          done = true;
          /*console.log(
            "found " + result.whichCompany + " for " + el.ip + " " + el.hosthash
          );*/
          cache[company]["hosts"].push(el.hosthash);
          cache["notInList"]["hosts"] = cache["notInList"]["hosts"].filter(
            function(ele) {
              return ele != el.hosthash;
            }
          );
        }
      }
    }
    /* if (done) {
      break;
    }*/
  }
  // console.log(cache.amazon.hosts);
  // console.log(cache.facebook.hosts);

  return cache;
};

CacheUpdateHelper.checkIPisinRange = (address, whichCompany) => {
  //  var a = performance.now()
  var result = {};
  result.whichCompany = "notInList";
  var toLookup = "";
  //looking up both addresses take much time, let's look up first one for now in this version
  toLookup = address;
  var parsed_addr = ipaddr.process(toLookup);
  var addressType = parsed_addr.kind();

  // for (var address of addresses) {
  if (
    check_many_cidrs(
      parsed_addr,
      addressType,
      addressranges[whichCompany][addressType]
    )
  ) {
    result.whichCompany = whichCompany;
  }
  // }
  //console.log((performance.now() - a)/1000)
  //console.log(result);
  return result;
};

self.onmessage = function(e) {
  //console.log(e.data);
  var obj = {};
  obj.workerResult = CacheUpdateHelper.handleCacheToggle(
    e.data[0],
    e.data[1],
    e.data[2]
  );
  postMessage(obj);
};
