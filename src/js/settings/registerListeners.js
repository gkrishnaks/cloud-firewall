/*******************************************************************************

    
    Cloud Firewall - a browser extension/addon that allows users to block connections 
    to sites, pages and web resources (images, videos, etc) hosted in major cloud services
    if the user wishes to do so. 
    Copyright (C) 2019 Gokulakrishna Sudharsan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Home: https://gitlab.com/gkrishnaks/cloud-firewall
*/

var App = window.App || {};
App.settingsApp = window.App.settingsApp || {};

window.onload = function() {
  /* document.querySelector("#enableEncryption").disabled = true;
  document.querySelector("#disableEncryption").disabled = true;
*/
  document.addEventListener("click", App.settingsApp.listenForClicks);
  document
    .getElementById("toggleOperationMode")
    .addEventListener("keyup", App.keyUpListener);
  document
    .getElementById("toggleLogging")
    .addEventListener("keyup", App.keyUpListener);
  document
    .getElementById("toggleRulePersistence")
    .addEventListener("keyup", App.keyUpListener);
  document
    .getElementById("toggleNotifications")
    .addEventListener("keyup", App.keyUpListener);

  document
    .getElementById("toggleDecentraleyes")
    .addEventListener("keyup", App.keyUpListener);

  /*  document
    .getElementById("password2")
    .addEventListener("input", App.settingsApp.comparePasswords);
   document
    .getElementById("password2")
    .addEventListener("keyup", App.settingsApp.comparePasswords);*/
};

App.settingsApp.comparePasswords = function(e) {
  let password1 = document.getElementById("password").value;
  if (e.target.value === password1) {
    document.querySelector("#enableEncryption").disabled = false;
    document.querySelector("#disableEncryption").disabled = false;
  } else {
    document.querySelector("#enableEncryption").disabled = true;
    document.querySelector("#disableEncryption").disabled = true;
  }
};

/* chrome.runtime.sendMessage(
    {
      type: "enableEncryption",
      subtype: "fromSettings",
      domain: "settings.html"
    },
    function(response) {
      console.log(response);
    });
*/

App.keyUpListener = function(e) {
  if (e.keyCode === 13 || e.key === "Enter") {
    switch (e.target.id) {
      case "password2":
        App.settingsApp.toggleEncryption(e.target.value);
        e.target.value = "";
        document.getElementById("password").value = "";

        break;
    }
  } else if (e.key === " " || e.keyCode === 32) {
    switch (e.target.id) {
      case "toggleLogging":
        App.settingsApp.toggleLogging();
        break;
      case "toggleOperationMode":
        App.settingsApp.toggleOperationMode();
        break;
      case "toggleRulePersistence":
        App.settingsApp.toggleRulePersistence();
        break;
      case "toggleNotifications":
        App.settingsApp.toggleNotifications();
        break;
      case "toggleDecentraleyes":
        App.settingsApp.toggleDecentraleyes();
        break;
    }
  }
  // App.settingsApp.getRules();
};

App.settingsApp.listenForClicks = function(e) {
  App.settingsApp.incExcmsg = null;

  switch (e.target.id) {
    case "enableEncryption":
    case "disableEncryption":
      let el3 = document.querySelector("#password2");
      App.settingsApp.toggleEncryption(el3.value);
      el3.value = "";
      document.querySelector("#password").value = "";
      break;

    case "toggleLogging":
      App.settingsApp.toggleLogging();
      break;
    case "toggleResetwarn":
      App.settingsApp.toggleResetwarn();
      document.getElementById("toggleResetwarn").style.display = "none";
      document.getElementById("doFactoryReset").scrollIntoView();
      break;
    case "cancelresets":
      App.settingsApp.toggleResetwarn();
      document.getElementById("showresetConfirmation").style.display = "none";
      document.getElementById("toggleResetwarn").style.display = "";
      break;
    case "doFactoryReset":
      App.settingsApp.doFactoryReset();
      break;
    case "toggleOperationMode":
      App.settingsApp.toggleOperationMode();
      break;
    case "openHelp":
      App.settingsApp.openHelp();
      break;
    case "toggleRulePersistence":
      App.settingsApp.toggleRulePersistence();
      break;
    case "toggleNotifications":
      App.settingsApp.toggleNotifications();
      break;
    case "toggleDecentraleyes":
      App.settingsApp.toggleDecentraleyes();
      break;
    case "decentraleyesNote":
      document.getElementById("NotetoDecentraleyesUsers").style.display = "";
      document.getElementById("NotetoDecentraleyesUsers").scrollIntoView();
      document.getElementById("decentraleyesNote").style.display = "none";
      break;
  }
};
